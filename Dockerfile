FROM debian:10

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq -y update \
&& apt-get -qq -y install gettext git tar gcc mime-support build-essential gnupg curl \
    libcurl4-openssl-dev libxslt1-dev libxml2-dev libspeex-dev \ 
    libogg-dev libvorbis-dev libflac-dev libtheora-dev libssl-dev \
&& curl -sL https://downloads.xiph.org/releases/icecast/icecast-2.4.4.tar.gz | tar -xzvf - \
&& cd icecast-2.4.4 \
&& ./configure --with-curl --with-openssl \
&& grep lssl config.status \
&& make \
&& make install \
&& ln -s /usr/local/bin/icecast /usr/bin/icecast2 \
&& ln -s /usr/local/share/icecast /usr/share/icecast2 \
&& rm -rf /var/lib/apt/lists/*

RUN addgroup --system icecast \
&& adduser --disabled-password --system --home /home/icecast2 --shell /sbin/nologin --ingroup icecast icecast2 \
&& sh -c "echo 'USERID=root\nGROUPID=root' >> /etc/default/icecast2" \
&& mkdir -p /etc/icecast2/ssl \
&& mkdir -p /var/log/icecast2/ \
&& chown -R icecast2 /etc/icecast2 /var/log/icecast2/

COPY bundle.pem.gpg /etc/icecast2/ssl/bundle.pem.gpg
COPY icecast.xml.template /etc/icecast2/icecast.xml.template

EXPOSE 80
# USER icecast2 # will use icecast changeuser directive
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT [ "/bin/bash", "/usr/local/bin/entrypoint.sh" ]
CMD ["icecast2", "-c", "/etc/icecast2/icecast.xml"]