#!/bin/bash
set -eo pipefail

# decrypt and populate files
echo "${BUNDLE_PEM_DECRYPTION_PASSWORD}" | gpg --decrypt --batch --yes --passphrase-fd 0 /etc/icecast2/ssl/bundle.pem.gpg > /etc/icecast2/ssl/bundle.pem
chmod 600 /etc/icecast2/ssl/bundle.pem
chown icecast2:icecast /etc/icecast2/ssl/bundle.pem
envsubst > /etc/icecast2/icecast.xml < /etc/icecast2/icecast.xml.template

# start main process
exec "$@"